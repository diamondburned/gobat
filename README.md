# gobat

Go library for pwoer-supply `uevent` files 

# Documentation

[![GoDoc](https://godoc.org/gitlab.com/diamondburned/gobat?status.svg)](https://godoc.org/gitlab.com/diamondburned/gobat)

# Reference

#### [gobat-cli](https://gitlab.com/diamondburned/gobat-cli)