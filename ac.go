package gobat

import (
	"log"

	ini "github.com/go-ini/ini"
)

// Populate updates information regarding pointer PowerSupply
func (ac *AC) Populate() error {
	path := ac.Path
	uevent, err := ini.Load(path + "/uevent")
	if err != nil {
		log.Println(err.Error())
		return err
	}

	log.Println(path + "/uevent")
	log.Println(uevent.Section("").Key("POWER_SUPPLY_NAME").String())

	*ac = AC{
		Path:   path,
		Name:   uevent.Section("").Key("POWER_SUPPLY_NAME").String(),
		Online: keyToBool(uevent.Section("").Key("POWER_SUPPLY_ONLINE")),
	}

	return nil
}
