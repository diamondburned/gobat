package gobat

import (
	"log"
	"strconv"
	"time"

	ini "github.com/go-ini/ini"
)

// Battery is the struct for a battery
type Battery struct {
	Path       string
	Name       string   `json:"name"`
	Status     string   `json:"status"`
	Present    bool     `json:"present"`
	Technology string   `json:"technology"`
	Model      Model    `json:"model"`
	CycleCount int64    `json:"cycle_count"`
	Voltage    Voltage  `json:"voltage"`
	CurrentNow int64    `json:"current_now"`
	Charge     Charge   `json:"charge"`
	Capacity   Capacity `json:"capacity"`
}

// Capacity is the battery capacity
type Capacity struct {
	Capacity int    `json:"capacity"`
	Level    string `json:"level"`
}

// Charge is the battery charge
type Charge struct {
	FullDesign int64 `json:"full_design"`
	Full       int64 `json:"full"`
	Now        int64 `json:"now"`
}

// Model is the battery model
type Model struct {
	Name         string `json:"name"`
	Manufacturer string `json:"manufacturer"`
	Serial       int64  `json:"serial"`
}

// Voltage is the battery voltage
type Voltage struct {
	MinDesign int64 `json:"min_design"`
	Now       int64 `json:"now"`
}

// Populate updates information regarding pointer PowerSupply
func (b *Battery) Populate() error {
	path := b.Path
	uevent, err := ini.Load(path + "/uevent")
	if err != nil {
		log.Println(err.Error())
		return err
	}

	*b = Battery{
		Path:       path,
		Name:       uevent.Section("").Key("POWER_SUPPLY_NAME").String(),
		Status:     uevent.Section("").Key("POWER_SUPPLY_STATUS").String(),
		Present:    keyToBool(uevent.Section("").Key("POWER_SUPPLY_PRESENT")),
		Technology: uevent.Section("").Key("POWER_SUPPLY_TECHNOLOGY").String(),
		Model: Model{
			Name:         uevent.Section("").Key("POWER_SUPPLY_MODEL_NAME").String(),
			Manufacturer: uevent.Section("").Key("POWER_SUPPLY_MANUFACTURER").String(),
			Serial:       keyToInt(uevent.Section("").Key("POWER_SUPPLY_SERIAL_NUMBER")),
		},
		CycleCount: keyToInt(uevent.Section("").Key("POWER_SUPPLY_CYCLE_COUNT")),
		Voltage: Voltage{
			MinDesign: keyToInt(uevent.Section("").Key("POWER_SUPPLY_VOLTAGE_MIN_DESIGN")),
			Now:       keyToInt(uevent.Section("").Key("POWER_SUPPLY_VOLTAGE_NOW")),
		},
		CurrentNow: keyToInt(uevent.Section("").Key("POWER_SUPPLY_CURRENT_NOW")),
		Charge: Charge{
			FullDesign: keyToInt(uevent.Section("").Key("POWER_SUPPLY_CHARGE_FULL_DESIGN")),
			Full:       keyToInt(uevent.Section("").Key("POWER_SUPPLY_CHARGE_FULL")),
			Now:        keyToInt(uevent.Section("").Key("POWER_SUPPLY_CHARGE_NOW")),
		},
		Capacity: Capacity{
			Capacity: int(keyToInt(uevent.Section("").Key("POWER_SUPPLY_CAPACITY"))),
			Level:    uevent.Section("").Key("POWER_SUPPLY_CAPACITY_LEVEL").String(),
		},
	}

	return nil
}

// IsCharging is a pretty redundant function, but I'll be using it
func (b *Battery) IsCharging() bool {
	return b.Status == "Charging"
}

// PercentCharge calculates the remaining power
func (b *Battery) PercentCharge() float64 {
	return float64(b.Charge.Now) / float64(b.Charge.Full) * 100.
}

// TimeRemaining calculates the time remaining
func (b *Battery) TimeRemaining() (*time.Duration, error) {
	h := float64(b.Charge.Now) / float64(b.CurrentNow) * 3600000000000.
	if b.Status == "Full" {
		h = 0
	}

	duration, err := time.ParseDuration(strconv.FormatFloat(h, 'f', -1, 64) + "ns")
	if err != nil {
		return nil, err
	}

	return &duration, nil
}

// TimeLeftUntilFull calculates the time left until full charge
func (b *Battery) TimeLeftUntilFull() (*time.Duration, error) {
	h := (float64(b.Charge.Full) - float64(b.Charge.Now)) / float64(b.CurrentNow) * 3600000000000.

	duration, err := time.ParseDuration(strconv.FormatFloat(h, 'f', -1, 64) + "ns")
	if err != nil {
		return nil, err
	}

	return &duration, nil
}
