package gobat

import (
	"io/ioutil"
	"log"
	"path/filepath"

	// "os"

	ini "github.com/go-ini/ini"
)

// PowerSupplies is the struct for batteries and AC
type PowerSupplies struct {
	AC      AC
	Battery []Battery
}

// AC is the struct for the power source
type AC struct {
	Path   string
	Name   string
	Online bool
}

// GetDevices returns an array of backlight devices (intel_backlight, acpi_backlight, etc)
func GetDevices() (*PowerSupplies, error) {
	// CHANGE ME TO NOT HAVE /tmp
	// powerSupplyPath := "/tmp/sys/class/power_supply/"
	powerSupplyPath := "/sys/class/power_supply/"

	dirs, err := ioutil.ReadDir(powerSupplyPath)
	if err != nil {
		return nil, err
	}

	batteries := []Battery{}
	ac := AC{}

	for _, d := range dirs {
		dest, err := filepath.EvalSymlinks(powerSupplyPath + d.Name())
		if err != nil {
			return nil, err
		}
		log.Println("Found", dest)

		switch d.Name() {
		case "AC":
			ac = AC{
				Path: dest,
			}
		default:
			battery := Battery{
				Path: dest,
			}

			batteries = append(batteries, battery)
		}
	}

	ps := &PowerSupplies{
		Battery: batteries,
		AC:      ac,
	}

	return ps, nil
}

// PopulateAll updates everything in PowerSupplies
func (devices *PowerSupplies) PopulateAll() error {
	ac := &AC{
		Path: devices.AC.Path,
	}
	if err := ac.Populate(); err != nil {
		return err
	}

	batteries := []Battery{}
	for _, battery := range devices.Battery {
		bat := &Battery{
			Path: battery.Path,
		}

		if err := bat.Populate(); err != nil {
			return err
		}

		batteries = append(batteries, *bat)
	}

	*devices = PowerSupplies{
		AC:      *ac,
		Battery: batteries,
	}

	return nil
}

func keyToBool(k *ini.Key) bool {
	switch k.String() {
	case "0":
		return false
	default: // not 0 aka 1
		return true
	}
}

func keyToInt(k *ini.Key) int64 {
	i, e := k.Int64()
	if e != nil {
		log.Println(e)
		i = 0
	}

	return i
}
